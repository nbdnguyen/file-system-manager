# File System Manager

A JAVA console application that can be used to manage and gather information on specific files.

Terminal commands:

* Compile to /out/: javac -d out src/main/java/*.java

![screenshot of compiling](screenshots/javac.png?raw=true "Javac");

* Create .jar file while in /out/: jar cfe Program.jar Program *.class

![screenshot of creating .jar](screenshots/jar.png?raw=true "Jar");

* Run .jar file: java -jar Program.jar

![screenshot of running .jar](screenshots/javajar.png?raw=true "java jar");

* Contains:

    1. User interaction

    2. File manipulation

    3. Logging

## User Interaction

Persisting menu with options and exit.

## File Manipulation

A file service that gathers information about files.

1. function: list all file names in given directory.

2. function: get specific files by their extension.

3. several functions: manipulate provided .txt:

    * Name
    * Size
    * Amount of lines
    * Singular word search
    * Amount of times word is used

## Logging

Write strings to file. Log all results from File Manipulation (option 4) and display message with a timestamp, and duration.

Example: 

* "2020/09/13 16:20:22: Got the name of the file Dracula.txt. The function took 9ms to execute."