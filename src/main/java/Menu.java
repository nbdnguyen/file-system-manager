import java.util.Scanner;
import java.io.*;

public class Menu {
    Service fileService = new Service();
    Scanner input = new Scanner(System.in);
    
    public void displayMenu() {
        System.out.println();
        System.out.println("Menu: ");
        System.out.println("1. Display menu");
        System.out.println("2. List all files in directory");
        System.out.println("3. List all files with same extension");
        System.out.println("4. Get information about a file");
        System.out.println("0. Exit");
        pick();
    }
    
    public void pick() {
        String number = ask();
        switch (number) {
            case "1":
                displayMenu();
                break;
            case "2":
                fileService.allFiles();
                pick();
                break;
            case "3":
                fileService.extFiles();
                pick();
                break;
            case "4":
                pickFile();
                break;
            case "0":
                close();
                break;
            default:
                System.out.println("Pick a valid option!");
                pick();
                break;
        }
    }
    
    public String ask() {
        System.out.print("Pick an option: ");
        String number = "";
        try {
            number = input.next();
        } catch(Exception e) {
            System.out.println("Error: That is not a valid input.");
        }
        return number;
    }

    public void close() {
        input.close();
        System.exit(1);
    }

    public String pickFile() {
        System.out.print("Which file? ");
        String name = "";
        try {
            name = "../src/main/resources/" + input.next();
        } catch(Exception e) {
            System.out.println("Error: could not find file.");
        }
        File filename = new File(name);
        while (name.equals("") || !(filename.exists())) {
            System.out.println("No valid filename given.");
            System.out.println();
            name = pickFile();
        }
        fileMenu(filename);
        return name;
    }

    public void fileMenu(File filename) {
        System.out.println();
        System.out.println("1. Display filemanipulation menu");
        System.out.println("2. Get filename");
        System.out.println("3. Get size of file");
        System.out.println("4. Amount of lines in file");
        System.out.println("5. Search for a word");
        System.out.println("6. Search for how often a word is used");
        System.out.println("7. Pick a different file");
        System.out.println("9. Back to main menu");
        System.out.println("0. Exit");
        pickFileOption(filename);
    }

    public void pickFileOption(File filename) {
        String number = ask();
        switch (number) {
            case "1":
                fileMenu(filename);
                break;
            case "2": 
                fileService.getFileName(filename);
                pickFileOption(filename);
                break;
            case "3":
                fileService.getFileSize(filename);
                pickFileOption(filename);
                break;
            case "4":
                fileService.lineAmount(filename);
                pickFileOption(filename);
                break;
            case "5":
                fileService.wordSearch(filename);
                pickFileOption(filename);
                break;
            case "6":
                fileService.wordUsage(filename);
                pickFileOption(filename);
                break;
            case "7":
                pickFile();
            case "9":
                displayMenu();
            case "0":
                fileService.close();
            default:
                System.out.println("Not a valid option!");
                pickFileOption(filename);
                break;
        }
    }
}
