import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;  

public class Logger {
    File logFile = new File("logfile.txt");
    FileWriter writer;
    PrintWriter printer;

    public Logger() {
        try {
            logFile.createNewFile();
            writer = new FileWriter(logFile);
            printer = new PrintWriter(writer);
        } catch(Exception e) {
            System.out.println("Error: Problems with the file.");
        }
    }

    public void logging(String intoFile) {
        try {
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
            LocalDateTime now = LocalDateTime.now();  
            printer.print(dtf.format(now) + ": " + intoFile + "\n");
        } catch(Exception e) {
            System.out.println("Error: Could not write to file.");
        }
    }

    public void endLogging() {
        try {
            writer.close();
            printer.close();
        } catch(Exception e) {
            System.out.println("Error: Could not close logfile.");
        }
    }
}
